import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { Repository, UpdateResult } from 'typeorm';
import { ProductDto } from './product.dto';
import { PutProductDto } from './products.controller';

export interface Paginate<T> {
  data: T[];
  count: number;
}

@Injectable()
export class ProductsService {
  constructor(@InjectRepository(Product) private productRepository: Repository<Product>) {}

  async find(query: {take: number, skip: number} = { take: 10, skip: 0}): Promise<Paginate<Product>> {
    const [data, count] = await this.productRepository.createQueryBuilder('products')
      .innerJoinAndSelect('products.price', 'price')
      .leftJoin('products.price', 'latest_price', 'price.id < latest_price.id')
      .where('latest_price.id IS NULL')
      .take(query.take)
      .skip(query.skip)
      .getManyAndCount();

    return { data, count };
  }

  /**
   * @name findById
   * @description Find a product with his latest price joined.
   * @param {number} id
   * @returns {Promise<Product>}
   */
  async findById(id: number): Promise<Product> {
    return await this.productRepository.createQueryBuilder('products')
      .innerJoinAndSelect('products.price', 'price')
      .leftJoin('products.price', 'latest_price', 'price.id < latest_price.id')
      .where('latest_price.id IS NULL AND products.id = :id', { id })
      .getOne();
  }

  /**
   * @description Create a new product.
   * @param {ProductDto} data Payload of the request.
   */
  async post(data: ProductDto): Promise<Product> {
    const entity = this.productRepository.create(data);
    return await this.productRepository.save(entity);
  }

  async put(product: PutProductDto): Promise<boolean> {
    const update = await this.productRepository.update(product.id, product);
    return update.affected > 0;
  }
}
