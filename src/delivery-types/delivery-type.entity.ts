import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { TimestampColumn } from '../embedded-entities/timestamp.column';
import { Delivery } from '../deliveries/delivery.entity';

export enum DeliveryTypeEnum {
  TAKE_AWAY = 1,
  DELIVRERY = 2
}

@Entity({ name: 'delivery_types' })
export class DeliveryType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @OneToMany(() => Delivery, delivery => delivery.deliveryType)
  deliveries: Delivery[];

  @Column(() => TimestampColumn)
  timestamp: TimestampColumn;
}
