import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Enroll } from './enroll.entity';
import { Repository } from 'typeorm';
import { CreateEnrollDto } from './dto/create-enroll.dto';

@Injectable()
export class EnrollService {
  constructor(@InjectRepository(Enroll) private enrollRepository: Repository<Enroll>) {}

  create(createEnrollDto: CreateEnrollDto): Promise<Enroll> {
    const enrollEntity = this.enrollRepository.create(createEnrollDto);
    return this.enrollRepository.save(enrollEntity);
  }
}
