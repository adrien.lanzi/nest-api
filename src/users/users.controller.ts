import { Controller, Get, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { UsersRo } from './users.ro';
import { User } from './user.decorator';
import { User as UserEntity } from './user.entity';
import { Customer } from '../customers/customer.decorator';
import { Customer as CustomerEntity } from '../customers/customer.entity';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(): Promise<UserEntity[]> {
    return this.usersService.findAll();
  }

  @ApiParam({ name: 'email' })
  @Get('email')
  async findOneByEmail(@Req() request: Request): Promise<UsersRo> {
    return this.usersService.findByEmail(request.body.email);
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async getProfile(@User() user: UserEntity): Promise<UsersRo> {
    return this.usersService.findOne(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('customer')
  getCustomer(@Customer() customer: CustomerEntity): CustomerEntity|null {
    return customer;
  }
}
