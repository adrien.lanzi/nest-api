import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { Order } from '../order.entity';

@ViewEntity({
  expression: (connection: Connection) => connection.createQueryBuilder()
    .select('order.id', 'id')
    .addSelect('order.customerId', 'customerId')
    .addSelect('delivery.deliveryDate', 'deliveryDate')
    .addSelect('SUM(price.amount)', 'total')
    .addSelect('deliveryType.label', 'deliveryType')
    .addSelect('deliveryStatus.label', 'deliveryStatus')
    .from(Order, 'order')
    .innerJoin('order.delivery', 'delivery')
    .leftJoin('order.delivery', 'latest_delivery', 'delivery.id < latest_delivery.id')
    .innerJoin('order.orderProducts', 'orderProducts')
    .innerJoin('orderProducts.product', 'products')
    .innerJoin('delivery.deliveryType', 'deliveryType')
    .innerJoin('delivery.deliveryStatus', 'deliveryStatus')
    .innerJoin('products.price', 'price')
    .leftJoin('products.price', 'latest_price', 'price.id < latest_price.id')
    .where('latest_price.id IS NULL')
    .andWhere('latest_delivery.id IS NULL')
    .groupBy('order.id')
    .addGroupBy('delivery.deliveryDate')
    .addGroupBy('deliveryType.label')
    .addGroupBy('deliveryStatus.label')
})
export class OrdersCustomerViewEntity {
  @ViewColumn()
  id: number;

  @ViewColumn()
  customerId: number;

  @ViewColumn()
  deliveryDate: Date;

  @ViewColumn()
  total: number;

  @ViewColumn()
  deliveryType: string;

  @ViewColumn()
  deliveryStatus: string;
}
