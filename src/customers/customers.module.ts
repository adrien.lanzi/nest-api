import { Module } from '@nestjs/common';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './customer.entity';
import { Address } from '../addresses/address.entity';
import { AddressesService } from '../addresses/addresses.service';
import { OrdersService } from '../orders/orders.service';
import { Order } from '../orders/order.entity';
import { ProductsService } from '../products/products.service';
import { Product } from '../products/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer, Address, Order, Product])],
  exports: [CustomersService],
  controllers: [CustomersController],
  providers: [CustomersService, AddressesService, OrdersService, ProductsService]
})
export class CustomersModule {}
