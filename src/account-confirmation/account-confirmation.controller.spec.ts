import { Test, TestingModule } from '@nestjs/testing';
import { AccountConfirmationController } from './account-confirmation.controller';

describe('AccountConfirmation Controller', () => {
  let controller: AccountConfirmationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccountConfirmationController],
    }).compile();

    controller = module.get<AccountConfirmationController>(AccountConfirmationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
