import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Price } from './price.entity';
import { Repository } from 'typeorm';
import { Product } from '../products/product.entity';

@Injectable()
export class PricesService {
  constructor(@InjectRepository(Price) private pricesRepository: Repository<Price>) {}

  async getPricesOfProduct(productId: number): Promise<Price[]> {
    return await this.pricesRepository.createQueryBuilder('prices')
      .innerJoinAndSelect(Product, 'product', 'product.id = :productId', { productId })
      .orderBy('created', 'DESC')
      .getMany();
  }

  async post(data: { amount: number, product: { id: number} }): Promise<Price> {
    return await this.pricesRepository.save(data);
  }
}
