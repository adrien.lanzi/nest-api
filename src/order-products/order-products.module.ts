import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderProduct } from './order-product.entity';
import { OrderProductService } from './order-product.service';


@Module({
  imports: [TypeOrmModule.forFeature([OrderProduct])],
  providers: [OrderProductService],
  exports: [OrderProductService],
  controllers: []
})
export class OrderProductsModule {}
