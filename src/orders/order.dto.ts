export class OrderDto {
  fees: number;
  customer_id: number;
  created: string;
  modified: string;
}
