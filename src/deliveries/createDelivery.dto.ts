import { DeliveryStatus } from '../delivery-status/delivery-status.entity';
import { DeliveryType } from '../delivery-types/delivery-type.entity';
import { Order } from '../orders/order.entity';
import { Address } from '../addresses/address.entity';

export class CreateDeliveryDto {
  deliveryDate: Date;
  deliveryStatus: DeliveryStatus;
  deliveryType: DeliveryType;
  order: Order;
  customerAddress: Address;
}
