import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User, UserRole } from './user.entity';
import { CreateUserDto } from './users.dto';
import { UsersRo } from './users.ro';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async findOne(id: number): Promise<UsersRo> {
    const user = await this.usersRepository.findOne(id);
    return user.toResponseObject();
  }

  async getByEmail(email: string): Promise<User|null> {
    return await this.usersRepository.findOne({ email });
  }

  /**
   * @description Find an user by his email.
   * @param {String} email Email of the user.
   */
  async findByEmail(email: string): Promise<UsersRo> {
    const user = await this.usersRepository.findOne({ email });
    return user.toResponseObject();
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }

  async create(user: CreateUserDto): Promise<User> {
    return await this.usersRepository.save(user);
  }

  async update(id: number, newValue: CreateUserDto): Promise<UsersRo | null> {
    const user = await this.usersRepository.findOneOrFail(id);
    if (!user.id) {
      console.error('This user do not exist');
    }
    await this.usersRepository.update(id, newValue);
    return await this.findOne(id);
  }

  async register(userDto: CreateUserDto): Promise<User> {
    const { email } = userDto;
    let user = await this.usersRepository.findOne({ email });
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }
    user = await this.usersRepository.create({ ...userDto, roles: [UserRole.CUSTOMER] });
    return await this.usersRepository.save(user);
  }

  async activate(user: User): Promise<User> {
    user.isActive = true;
    return await this.usersRepository.save(user);
  }
}
