import { Module } from '@nestjs/common';
import { DeliveryStatusController } from './delivery-status.controller';

@Module({
  controllers: [DeliveryStatusController]
})
export class DeliveryStatusModule {}
