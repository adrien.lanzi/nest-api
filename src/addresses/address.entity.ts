import { Column, Entity, Index, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Customer } from '../customers/customer.entity';
import { Delivery } from '../deliveries/delivery.entity';

@Entity({ name: 'addresses' })
@Index(['name', 'customer'], { unique: true })
export class Address {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ length: 5 })
  zipCode: string;

  @Column()
  city: string;

  @Column({ length: 10 })
  phoneNumber: string;

  @Column()
  streetNumber: string;

  @Column()
  streetName: string;

  @Column({ default: true })
  active: boolean;

  @Column({ type: 'varchar', nullable: false, name: 'customerId' })
  @ManyToOne(() => Customer, customer => customer.addresses)
  customer: Customer;

  @OneToMany(() => Delivery, delivery => delivery.address)
  deliveries: Delivery[]
}
