import { Module } from '@nestjs/common';
import { AccountConfirmationService } from './account-confirmation.service';
import { AccountConfirmationController } from './account-confirmation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountConfirmation } from './account-confirmation.entity';

@Module({
  imports: [TypeOrmModule.forFeature([AccountConfirmation])],
  providers: [AccountConfirmationService],
  controllers: [AccountConfirmationController],
  exports: [AccountConfirmationService]
})
export class AccountConfirmationModule {}
