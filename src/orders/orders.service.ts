import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository, SelectQueryBuilder, UpdateResult } from 'typeorm';
import { Order } from './order.entity';
import { OrderDto } from './order.dto';
import { CreateOrderDto } from './createOrderDto';
import { Customer } from '../customers/customer.entity';
import { Product } from '../products/product.entity';
import { Paginate, ProductsService } from '../products/products.service';
import { User } from '../users/user.entity';
import { DeliveryStatusEnum } from '../delivery-status/delivery-status.entity';
import { DeliveryTypeEnum } from '../delivery-types/delivery-type.entity';
import { Address } from '../addresses/address.entity';
import { OrdersCustomerViewEntity } from './views/ordersCustomer.view.entity';
import { OrdersHelper } from './orders.helper';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(Customer) private customersRepository: Repository<Customer>,
    @InjectRepository(Product) private productRepository: Repository<Product>,
    @InjectRepository(Address) private addressRepository: Repository<Address>,
    private productsService: ProductsService
  ) {}

  /**
   * @description Find all orders.
   * @param { take: number, skip: number } [query]
   */
  async findPaginated(query: { take: number, skip: number } = { take: 10, skip: 0 }): Promise<Paginate<Order>> {
    const [data, count] = await this.ordersRepository.findAndCount({ take: query.take, skip: query.skip * query.take, relations: ['orderProducts', 'orderProducts.product', 'orderProducts.price', 'customer'] });
    return { data, count };
  }

  /**
   * @description Find an order by id
   * @param {Number} id
   */
  async findById(id: number): Promise<Order> {
    return await this.ordersRepository.findOne(id);
  }

  /**
   * @description Find the orders of a customer.
   * @param {Number} customerId ID of the customer.
   * @param { take: Number, skip: Number } [query]
   */
  async findByCustomer(customerId: number, query: { take: number, skip: number } = { take: 10, skip: 0 }): Promise<any> {
    /*const [data, count] = await this.ordersRepository
      .createQueryBuilder('orders')
      .select('SUM(price.amount)', 'sum')
      .addSelect(['orders'])
      .groupBy('orders.id')
      .addGroupBy('delivery.id')
      .addGroupBy('orderProducts.id')
      .innerJoinAndSelect('orders.delivery', 'delivery')
      .leftJoin('orders.delivery', 'latest_delivery', 'delivery.id < latest_delivery.id')
      .innerJoinAndSelect('orders.orderProducts', 'orderProducts')
      .innerJoinAndSelect('orderProducts.product', 'product')
      .innerJoinAndSelect('orderProducts.price', 'price')
      .innerJoinAndSelect('delivery.deliveryStatus', 'deliveryStatus')
      .innerJoinAndSelect('delivery.deliveryType', 'deliveryType')
      .leftJoinAndSelect('delivery.address', 'address')
      .where('latest_delivery.id IS NULL')
      .andWhere('orders.customerId = :customerId', { customerId })
      .skip(query.skip)
      .take(query.take)
      .getManyAndCount();

    return { data, count };*/

    const [data, count] = await getManager().findAndCount(OrdersCustomerViewEntity, { where: { customerId: customerId }, order: { deliveryDate: 'DESC' } });

    return { data, count };
  }

  async create(user: User, createOrderDto: CreateOrderDto): Promise<Order> {

    let address = null;
    if (!Array.isArray(createOrderDto.orderProducts) || !createOrderDto.orderProducts.length) {
      // The order have no product.
      throw new HttpException(`No product in this order`, HttpStatus.BAD_REQUEST);
    }

    await Promise.all(createOrderDto.orderProducts.map(async orderedProduct => {
      const productId = orderedProduct.product.id;
      const priceId = orderedProduct.price.id;

      const productEntity = await this.productsService.findById(productId);

      if (!productEntity) {
        // One of the product ordered is not in database.
        throw new HttpException(`Product with id ${productId} does not exist`, HttpStatus.BAD_REQUEST);
      }

      if (productEntity.price.id !== priceId) {
        // The product is ordered with the wrong price.
        // What is wrong : the current price of the product is not the one submitted by the user.
        // We must tell the customer that the price has changed between his cart and the database.
        throw new HttpException(`Product with id ${productId} have price id ${productEntity.price.id} not ${priceId}`, HttpStatus.BAD_REQUEST);
      }
    }))

    const customerEntity = await this.customersRepository.findOne({ where: { user: user.id } });

    if (createOrderDto.delivery.deliveryType == DeliveryTypeEnum.DELIVRERY ) {
      if (!createOrderDto.delivery.addressId) {
        // The customer choose delivery but did not provided the address to be delivered.
        throw new HttpException(`You must provide an address to be delivered`, HttpStatus.BAD_REQUEST);
      }

      const customerAddresses: Address[] = await this.addressRepository.find({ where: { customer: customerEntity.id } })
      if (!customerAddresses.find(address => address.id === createOrderDto.delivery.addressId)) {
        // This address does not belong to the customer.
        throw new HttpException(`Bad customer address`, HttpStatus.BAD_REQUEST);
      }

      address = { id: createOrderDto.delivery.addressId }
    }

    const orderEntity = this.ordersRepository.create({
      fees: 0,
      orderProducts: createOrderDto.orderProducts,
      deliveries: [{
        deliveryDate: createOrderDto.delivery.deliveryDate,
        deliveryType: { id: createOrderDto.delivery.deliveryType },
        deliveryStatus: { id: DeliveryStatusEnum.PENDING },
        address: address
      }],
      customer: customerEntity
    });
    return await this.ordersRepository.save(orderEntity);
  }

  async findByIdForCustomer(id: string, customer: Customer): Promise<Order> {
    const queryBuilder = this.ordersRepository.createQueryBuilder('order');
    OrdersHelper.joinAndSelectMostRecentDelivery<Order>(queryBuilder);
    queryBuilder
      .innerJoinAndSelect('order.orderProducts', 'orderProducts')
      .innerJoinAndSelect('orderProducts.product', 'product')
      .innerJoinAndSelect('orderProducts.price', 'price')
      .innerJoinAndSelect('delivery.deliveryStatus', 'ds')
      .where('order.id = :id', { id })
      .andWhere('customerId = :customerId', { customerId: customer.id });

    return queryBuilder.getOne();
  }

  /**
   * @description Update an order.
   * @param {Number} id ID of the order to update.
   * @param {OrderDto} orderDto Data to update order with.
   */
  async updateOrder(id: number, orderDto: OrderDto): Promise<UpdateResult> {
    return await this.ordersRepository.update(id, orderDto);
  }
}
