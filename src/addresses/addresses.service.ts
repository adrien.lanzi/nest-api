import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Address } from './address.entity';
import { Repository } from 'typeorm';
import { Customer } from '../customers/customer.entity';
import { AddressDto } from './address.dto';

@Injectable()
export class AddressesService {
  constructor(@InjectRepository(Address) private addressesRepository: Repository<Address>) {}

  async post(payload: AddressDto): Promise<Address> {
    try {
      return await this.addressesRepository.save(payload);
    } catch (error) {
      return error.errno;
    }
  }

  async getAddressesByCustomerId(customerId: number): Promise<Address[]> {
    return await this.addressesRepository.find({ where: { customer: { id: customerId }, active: true } });
  }

  async deactivateForCustomer(addressId: string, customer: Customer): Promise<boolean> {
    const where = { id: addressId, customer: customer.id };
    return this.deactivate(where);
  }

  async deactivate(where: Record<string, any>): Promise<boolean> {
    const update = await this.addressesRepository.update({ ...where , active: true }, { active: false });
    return update.affected > 0;
  }
}
