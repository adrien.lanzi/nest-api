import { Body, Controller, Post } from '@nestjs/common';
import { AppController } from '../app.controller';
import { EnrollService } from './enroll.service';
import { CreateEnrollDto } from './dto/create-enroll.dto';

@Controller('enroll')
export class EnrollController extends AppController{
  constructor(private enrollService: EnrollService) {
    super();
  }

  @Post()
  async create(@Body() createEnrollDto: CreateEnrollDto): Promise<boolean> {
    try {
      await this.enrollService.create(createEnrollDto);
    } catch (e) { }

    return true;
  }
}
