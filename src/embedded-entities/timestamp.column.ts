import { Column } from 'typeorm';

export class TimestampColumn {
  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: Date;

  @Column({ type: 'timestamp', onUpdate: 'NOW()', nullable: true })
  modified: Date;

  // TODO : Maybe replace the @Column with specials columns @CreateDateColumn and @UpdateDateColumn
}
