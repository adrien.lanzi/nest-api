import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AppController } from '../app.controller';
import { PricesService } from './prices.service';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Price } from './price.entity';

@ApiTags('prices')
@Controller('prices')
export class PricesController extends AppController {
  constructor(private pricesService: PricesService) {
    super();
  }

  @UseGuards(JwtAuthGuard)
  @Get('product/:id')
  async getPricesOfProduct(@Param() params: { id: number }): Promise<Price[]> {
    const productId = params.id;
    return await this.pricesService.getPricesOfProduct(productId);
  }
}
