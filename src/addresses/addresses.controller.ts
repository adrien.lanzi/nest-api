import { Controller, Delete, Param, UseGuards } from '@nestjs/common';
import { AddressesService } from './addresses.service';
import { AppController } from '../app.controller';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Customer as CustomerEntity } from '../customers/customer.entity';
import { Customer } from '../customers/customer.decorator';

@Controller('addresses')
export class AddressesController extends AppController{
  constructor(private addressesService: AddressesService) {
    super();
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deactivateForCustomer(@Param('id') id: string, @Customer() customer: CustomerEntity): Promise<boolean> {
    return await this.addressesService.deactivateForCustomer(id, customer);
  }
}
