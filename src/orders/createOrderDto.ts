import { OrderProduct } from '../order-products/order-product.entity';

export class CreateOrderDto {
  orderProducts: OrderProduct[];
  delivery: {
    deliveryDate: Date;
    deliveryType: number;
    addressId: number
  }
}
