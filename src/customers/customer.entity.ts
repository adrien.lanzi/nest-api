import { AppEntity } from '../app.entity';
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/user.entity';
import { Address } from '../addresses/address.entity';
import { Order } from '../orders/order.entity';

@Entity({ name: 'customers' })
export class Customer extends AppEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ default: false })
  isRestricted: boolean;

  @OneToOne(type => User, user => user.customer)
  @JoinColumn()
  user: User;

  @OneToMany(() => Order, order => order.customer)
  orders: Order[];

  @OneToMany(type => Address, address => address.customer)
  addresses: Address[];

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: string;

  @Column({ type: 'timestamp', onUpdate: 'NOW()', nullable: true })
  modified: string;
}
