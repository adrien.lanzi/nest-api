import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Delivery } from './delivery.entity';
import { Paginate } from '../products/products.service';
import { CreateDeliveryDto } from './createDelivery.dto';
import { DeliveryStatus } from '../delivery-status/delivery-status.entity';

@Injectable()
export class DeliveriesService {
  constructor(@InjectRepository(Delivery) private deliveriesRepository: Repository<Delivery>, @InjectRepository(DeliveryStatus) private deliveryStatus: Repository<DeliveryStatus>) {}

  async findPaginated(query: { take: number, skip: number } = { take: 10, skip: 0 }): Promise<Paginate<Delivery>> {
    const [data, count] = await this.deliveriesRepository.findAndCount( { take: query.take, skip: query.skip * query.take, relations: ['order', 'deliveryType', 'deliveryStatus'] });

    return { data, count };
  }

  async create(data: CreateDeliveryDto): Promise<Delivery> {
    const entity = this.deliveriesRepository.create(data);

    return this.deliveriesRepository.save(entity);
  }

  async cancel(deliveryId: number): Promise<Delivery> {
    const deliveryEntity = await this.deliveriesRepository.findOne(deliveryId);
    deliveryEntity.deliveryStatus = await this.deliveryStatus.findOne(5);

    return await this.deliveriesRepository.save(deliveryEntity);
  }
}
