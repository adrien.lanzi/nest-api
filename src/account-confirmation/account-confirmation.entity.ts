import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '../users/user.entity';

@Entity({ name: 'account-confirmation' })
export class AccountConfirmation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column()
  expirationDate: Date;

  @ManyToOne(type => User, user => user.accountConfirmation)
  user: User;
}
