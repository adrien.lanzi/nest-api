/*import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { Order } from '../orders/order.entity';

@ViewEntity({
  expression: (connection: Connection) => connection.createQueryBuilder()
    .select('order.id', 'id')
    .addSelect('delivery.deliveryDate', 'deliveryDate')
    .addSelect('SUM(prices.amount)', 'price')
    .addSelect('deliveryType.label', 'deliveryType')
    .addSelect('deliveryStatus.label', 'deliveryStatus')
    .from(Order, 'order')
    .innerJoin('order.delivery', 'delivery')
    .innerJoin('order.orderProducts', 'orderProducts')
    .innerJoin('orderProducts.product', 'products')
    .innerJoin('delivery.deliveryType', 'deliveryType')
    .innerJoin('delivery.deliveryStatus', 'deliveryStatus')
    .innerJoinAndSelect('products.price', 'price')
    .leftJoin('products.price', 'latest_price', 'price.id < latest_price.id')
    .where('latest_price.id IS NULL AND products.id = :id', { id })
})
export class CustomerOrder {
  @ViewColumn()
  id: number;

  @ViewColumn()
  deliveryDate: Date;

  @ViewColumn()
  price: number;

  @ViewColumn()
  deliveryType: string;

  @ViewColumn()
  deliveryStatus: string;
}
*/
