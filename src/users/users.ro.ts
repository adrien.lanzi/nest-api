import { UserRole } from './user.entity';

export class UsersRo {
  id: number;
  email: string;
  isActive: boolean;
  roles: UserRole[]
}
