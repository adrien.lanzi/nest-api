import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { AppController } from '../app.controller';
import { DeliveriesService } from './deliveries.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Paginate } from '../products/products.service';
import { Delivery } from './delivery.entity';
import { CreateDeliveryDto } from './createDelivery.dto';

@Controller('deliveries')
export class DeliveriesController extends AppController {
  constructor(private deliveriesService: DeliveriesService) {
    super();
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async getPaginated(@Query() { take, skip }): Promise<Paginate<Delivery>> {
    return await this.deliveriesService.findPaginated({ take, skip});
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() body: CreateDeliveryDto): Promise<Delivery> {
    return await this.deliveriesService.create(body);
  }
}
