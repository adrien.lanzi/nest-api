import { Column, Entity, OneToMany, ManyToOne, PrimaryGeneratedColumn, OneToOne, UpdateDateColumn } from 'typeorm';
import { OrderProduct } from '../order-products/order-product.entity';
import { Customer } from '../customers/customer.entity';
import { TimestampColumn } from '../embedded-entities/timestamp.column';
import { Delivery } from '../deliveries/delivery.entity';
import { IsNotEmpty } from 'class-validator';

@Entity({ name: 'orders' })
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  fees: number;

  @OneToMany(() => OrderProduct, orderProduct => orderProduct.order, { cascade: ['insert'] })
  orderProducts: OrderProduct[];

  @ManyToOne(() => Customer, customer => customer.orders, { cascade: ['insert'] })
  customer: Customer;

  @OneToOne(() => Delivery, delivery => delivery.order, { cascade: ['insert'] })
  delivery: Delivery;

  @OneToMany(() => Delivery, delivery => delivery.order, { cascade: ['insert'] })
  deliveries: Delivery[];

  @Column(() => TimestampColumn)
  timestamp: TimestampColumn;
}

