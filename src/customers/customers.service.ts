import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './customer.entity';
import { Repository } from 'typeorm';
import { Paginate } from '../products/products.service';

@Injectable()
export class CustomersService {
  constructor(@InjectRepository(Customer) private customersRepository: Repository<Customer>) {}

  async get(id: number): Promise<Customer> {
    return await this.customersRepository.findOne(id);
  }

  async getAll(): Promise<Paginate<Customer>> {
    const [data, count] = await this.customersRepository.findAndCount();

    return { data, count };
  }

  async findByUserId(userId: number): Promise<Customer> {
    return await this.customersRepository.findOne({ where: { user: { id: userId } } });
  }

  /**
   * @description Find a customer by ID.
   * @param {Number} id ID of the customer.
   */
  async findById(id: number): Promise<Customer> {
    return await this.customersRepository.findOne(id);
  }

  async setRestriction(id: number, isRestricted: boolean): Promise<boolean> {
    const update = await this.customersRepository.update({ id }, { isRestricted } );
    return update.affected > 0;
  }
}
