import { ApiTags } from '@nestjs/swagger';
import { Body, Controller, HttpStatus, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/users.dto';
import { LocalAuthGuard } from './local-auth.guard';
import { Request, Response } from 'express';
import { AccountConfirmationService } from '../account-confirmation/account-confirmation.service';
import { UsersService } from '../users/users.service';
import { User } from '../users/user.decorator';
import { User as UserEntity } from '../users/user.entity';
import { Customer } from '../customers/customer.decorator';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly accountConfirmationService: AccountConfirmationService, private readonly usersService: UsersService) {
  }

  @Post('register')
  public async register(@Res() response: Response, @Body() createUserDto: CreateUserDto): Promise<Response> {
    const result = await this.authService.register(createUserDto);
    if (!result.success) {
      return response.status(HttpStatus.BAD_REQUEST).json(result);
    }
    return response.status(HttpStatus.OK).json(result);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@User() user: UserEntity): Promise<{ access_token: string }> {
    return this.authService.login(user);
  }

  @Post('confirmation')
  async confirmation(@Req() request: Request): Promise<boolean> {
    const token = request.body.token;
    const verify = await this.accountConfirmationService.verify(token);

    if (verify) {
      return await this.usersService.activate(verify.user).then(() => {
        return true;
      });
    } else {
      return false;
    }
  }
}
