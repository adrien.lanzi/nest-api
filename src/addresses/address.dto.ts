export class AddressDto {
  name: string;
  streetNumber: string;
  streetName: string;
  zipCode: string;
  phoneNumber: string;
  city: string;
  customerId?: string;
  active?: boolean;
}
