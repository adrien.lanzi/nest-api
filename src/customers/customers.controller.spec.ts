import { Test, TestingModule } from '@nestjs/testing';
import { CustomersController } from './customers.controller';
import { CustomersService } from './customers.service';
import { Customer } from './customer.entity';
import createSpyObj = jasmine.createSpyObj;

describe('Customers Controller', () => {
  let controller: CustomersController;
  let service: CustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CustomersController],
      providers: [CustomersService],
    }).compile();

    controller = module.get<CustomersController>(CustomersController);
    service = module.get<CustomersService>(CustomersService);
  });

  describe('findOne', () => {
    it('should return a customer', async () => {
      const id = 1;
      const result = new Customer() ;
      jest.spyOn(service, 'findById').mockImplementation(async () => await result);

      expect(await controller.getOne(id)).toBe(result);
    })
  })
});
