import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountConfirmation } from './account-confirmation.entity';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid'
import { User } from '../users/user.entity';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class AccountConfirmationService {
  constructor(@InjectRepository(AccountConfirmation) private accountConfirmationRepository: Repository<AccountConfirmation>, private readonly mailerService: MailerService) {
  }

  /**
   * @description Check the token of account confirmation.
   * @name verify
   * @param {string} token
   * @returns {Promise<boolean>}
   */
  public async verify(token: string): Promise<AccountConfirmation|false> {
    return this.accountConfirmationRepository.findOne({ token }, { relations: ['user'] }).then(response => {
      return (response && new Date(response.expirationDate) >= new Date() && response.user.isActive === false) ? response : false;
    });
  }

  public async create(user: User): Promise<AccountConfirmation> {
    const date = new Date();
    date.setHours(date.getHours() + 1)
    const data: { user: User, token: string, expirationDate: Date } = {
      user: user,
      token: uuidv4(),
      expirationDate: date
    };
    const entity = this.accountConfirmationRepository.create(data);
    return this.accountConfirmationRepository.save(entity).then(response => {
      this.sendEmail(data);
      return response;
    });
  }

  public async sendEmail(data: { user: User, token: string, expirationDate: Date }): Promise<boolean> {
    const link = process.env.CONFIRMATION_URL + '?t=' + data.token;

    return this.mailerService.sendMail({
      to: data.user.email,
      subject: 'Confirmer votre compte',
      template: './templates/account-confirmation',
      context: { firstName: data.user.customer.firstName, link }
    })
      .then(() => true)
      .catch((err) => {
        console.log(err);
        return false;
      });
  }
}
