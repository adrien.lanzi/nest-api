import { BeforeInsert, Column, Entity, OneToMany, PrimaryGeneratedColumn, OneToOne, Index } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UsersRo } from './users.ro';
import { Customer } from '../customers/customer.entity';
import { AccountConfirmation } from '../account-confirmation/account-confirmation.entity';

export enum UserRole {
  ADMIN = '1',
  CUSTOMER = '2'
}

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Index({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ type: "set", enum: UserRole, default: [UserRole.CUSTOMER] })
  roles: UserRole[]

  @Column({ default: false })
  isActive: boolean

  @OneToOne(() => Customer, customer => customer.user, { cascade: true })
  customer: Customer;

  @OneToMany(() => AccountConfirmation, accountConfirmation => accountConfirmation.user, { cascade: true })
  accountConfirmation: AccountConfirmation;

  @BeforeInsert()
  async hashPassword(): Promise<void> {
    this.password = await bcrypt.hash(this.password, 3);
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }

  toResponseObject(showToken = true): UsersRo {
    const { id, email, isActive, roles } = this;

    return { id, email, isActive, roles };
  }

  hasRole(role: UserRole) {
    return this.roles.indexOf(role) >= 0;
  }
}
