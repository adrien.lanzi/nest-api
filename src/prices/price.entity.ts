import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Product } from '../products/product.entity';
import { OrderProduct } from '../order-products/order-product.entity';

@Entity({ name: 'prices' })
export class Price {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @ManyToOne(() => Product, product => product.prices)
  @JoinColumn()
  product: Product;

  @OneToMany(() => OrderProduct, orderProduct => orderProduct.price)
  orderProduct: OrderProduct;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: string;

  @Column({ type: 'timestamp', onUpdate: 'NOW()', nullable: true })
  modified: string;
}

