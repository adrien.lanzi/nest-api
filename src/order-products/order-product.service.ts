import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderProduct } from './order-product.entity';
import { Connection, Repository } from 'typeorm';

@Injectable()
export class OrderProductService {
  constructor(
    private connection: Connection,
    @InjectRepository(OrderProduct) private orderProductRepository: Repository<OrderProduct>
  ) {}
}
