import { SelectQueryBuilder } from 'typeorm';

export class OrdersHelper {
  public static joinAndSelectMostRecentDelivery<T>(queryBuilder: SelectQueryBuilder<T>): void {
    queryBuilder
      .innerJoin('order.delivery', 'delivery')
      .leftJoin('order.delivery', 'mostRecentDelivery', 'delivery.id < mostRecentDelivery.id')
      .andWhere('mostRecentDelivery.id IS NULL');
  }

  public static joinAndSelectMostRecentPrice<T>(queryBuilder: SelectQueryBuilder<T>): void {
    queryBuilder
      .innerJoinAndSelect('order.orderProducts', 'orderProducts')
      .innerJoinAndSelect('orderProducts.product', 'product')
      .innerJoinAndSelect('product.price', 'price')
      .leftJoin('product.price', 'mostRecentPrice', 'price.id < mostRecentPrice.id')
      .andWhere('mostRecentPrice.id IS NULL')
  }
}
