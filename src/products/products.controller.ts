import {
  Body,
  Controller,
  Get,
  Param,
  Post, Put,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Paginate, ProductsService } from './products.service';
import { Product } from './product.entity';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { AppController } from '../app.controller';
import { ProductDto } from './product.dto';
import { Price } from '../prices/price.entity';
import { PricesService } from '../prices/prices.service';
import { Roles } from '../roles.decorator';
import { RolesGuard } from '../roles.guard';
import { UserRole } from '../users/user.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as Path from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Request, Response } from 'express';
import { UpdateResult } from 'typeorm';

export const buildFilename = (req: Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void): void => {
  const uuid = uuidv4();
  const fileExtName = Path.extname(file.originalname);
  callback(null, `${uuid}${fileExtName}`);
};

@ApiTags('products')
@Controller('products')
export class ProductsController extends AppController {
  constructor(private productsService: ProductsService, private pricesService: PricesService) {
    super();
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findPaginated(@Query() { take, skip }): Promise<Paginate<Product>> {
    return await this.productsService.find({ take, skip });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findById(@Param() params: { id: number }): Promise<Product> {
    return await this.productsService.findById(params.id);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @UseInterceptors(FileInterceptor('file', { storage: diskStorage({ destination: './uploads', filename: buildFilename }) }))
  @Post()
  async post(@Body() body: ProductDto, @UploadedFile() file: Express.Multer.File): Promise<Product> {
    const payload = { ...body, filename: file.filename }
    return await this.productsService.post(payload);
  }

  @Get('images/:fileId')
  async serveImage(@Param('fileId') fileId: string, @Res() res: Response): Promise<any> {
    res.sendFile(fileId, { root: 'uploads' });
  }

  @UseGuards(JwtAuthGuard)
  @Post(':id/prices')
  async addPrice(@Param() params: { id: number }, @Body() body: { amount: number }): Promise<Price> {
    return await this.pricesService.post({...body, product: { id: params.id } });
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @UseInterceptors(FileInterceptor('file', { storage: diskStorage({ destination: './uploads', filename: buildFilename }) }))
  @Put()
  async put(@Body() body: PutProductDto, @UploadedFile() file: Express.Multer.File): Promise<boolean> {
    const payload = { ...body }

    console.log(body, file);

    if (file) {
      return await this.uploadProductImage(payload, file);
    }

    delete payload.file;

    return await this.productsService.put(payload);
  }

  private async uploadProductImage(body: PutProductDto, @UploadedFile() file: Express.Multer.File): Promise<boolean> {
    const payload = { ...body, filename: file.filename }
    return await this.productsService.put(payload);
  }
}

export interface PutProductDto {
  id: number;
  label?: string;
  price?: Price;
  file?: string;
  filename?: string;
  available?: boolean;
}


