import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { jwtConstants } from './constants';
import { Customer } from '../customers/customer.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret
    });
  }

  /**
   * When an user log in, the JWT is fed with users data, the validate method retrieve those data.
   * @param payload
   */
  async validate(payload: any): Promise<{ id: string, email: string, roles: [], customer: Customer | null }> {
    return { id: payload.sub, email: payload.email, roles: payload.roles, customer: payload.customer || null }
  }
}

