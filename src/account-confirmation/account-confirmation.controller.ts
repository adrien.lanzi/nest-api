import { Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AccountConfirmationService } from './account-confirmation.service';
import { AccountConfirmation } from './account-confirmation.entity';

@ApiTags('AccountConfirmation')
@Controller('account-confirmation')
export class AccountConfirmationController {
  constructor(private accountConfirmationService: AccountConfirmationService) {
  }

  /*@Post()
  async post(userId: number|string): Promise<AccountConfirmation> {
    return await this.accountConfirmationService.create(userId);
  }*/
}
