import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User, UserRole } from '../users/user.entity';
import { CreateUserDto } from '../users/users.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { AccountConfirmationService } from '../account-confirmation/account-confirmation.service';
import { UsersRo } from '../users/users.ro';
import { CustomersService } from '../customers/customers.service';
import { Customer } from '../customers/customer.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly mailerService: MailerService,
    private usersService: UsersService,
    private jwtService: JwtService,
    private accountConfirmationService: AccountConfirmationService,
    private customersService: CustomersService) {}

  async validateUser(username: string, password: string): Promise<UsersRo|false|null> {
    const user = await this.usersService.getByEmail(username);
    if (user && await user.comparePassword(password)) {
      if (user.isActive) {
        const { password, ...result } = user; // Crop the password from the response.
        return result;
      }
      return false;
    }
    return null;
  }

  async login(user: User): Promise<{ access_token: string, user: User, customer?: Customer }> {
    const payload = { email: user.email, sub: user.id, roles: user.roles };

    if (user.roles.indexOf(UserRole.CUSTOMER) >= 0) {
      const customer = await this.customersService.findByUserId(user.id);
      Object.assign(payload, { customer });
    }

    return { access_token: this.jwtService.sign(payload), user };
  }

  async register(user: CreateUserDto): Promise<{ success: boolean, message: string }>{
    let status = {
      success: true,
      message: 'User register'
    };

    try {
      await this.usersService.register(user).then((user: User) => {
        this.accountConfirmationService.create(user);
      });
    } catch (error) {
      console.log(error);
      status = { success: false, message: error }
    }

    return status;
  }

  public async send(body): Promise<boolean> {

    return this.mailerService.sendMail({
      to: body.email,
      subject: 'Confirmation de votre compte',
      template: 'account-confirmation',
      context: body
    })
      .then(() => true)
      .catch((err) => {
        console.log(err);
        return false;
      });
  }
}
