export class CustomerDto {
  firstName: string;
  lastName: string;
  isRestricted: boolean;
}
