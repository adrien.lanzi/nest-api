export enum Role {
  User = 'user',
  Customer = 'customer',
  Admin = 'admin'
}
