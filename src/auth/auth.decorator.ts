import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { Customer } from '../customers/customer.entity';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { RolesGuard } from '../roles.guard';

export function Auth(customer: Customer) {
  return applyDecorators(
    SetMetadata('customer', customer),
    UseGuards(AuthGuard, RolesGuard),
    ApiBearerAuth(),
    ApiUnauthorizedResponse({ description: 'Unauthorized' })
  );
}
