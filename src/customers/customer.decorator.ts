import { createParamDecorator, ExecutionContext } from '@nestjs/common';

/**
 * @decorator Customer
 * @description Custom decorator that return the Customer object of the current user or null
 * The data are on the request, from the jwtStrategy's validate function.
 */
export const Customer = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user.customer;
  }
)
