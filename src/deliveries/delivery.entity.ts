import { Column, Entity, Index, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { DeliveryStatus } from '../delivery-status/delivery-status.entity';
import { Order } from '../orders/order.entity';
import { Address } from '../addresses/address.entity';
import { TimestampColumn } from '../embedded-entities/timestamp.column';
import { DeliveryType } from '../delivery-types/delivery-type.entity';

@Entity({ name: 'deliveries' })
export class Delivery {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  deliveryDate: Date;

  @ManyToOne(() => DeliveryStatus, deliveryStatus => deliveryStatus.delivery)
  deliveryStatus: DeliveryStatus

  @ManyToOne(() => DeliveryType, deliveryType => deliveryType.deliveries)
  deliveryType: DeliveryType

  @ManyToOne(() => Order, order => order.deliveries)
  order: Order

  @ManyToOne(() => Address, address => address.deliveries)
  address: Address

  @Column(() => TimestampColumn)
  timestamp: TimestampColumn;
}
