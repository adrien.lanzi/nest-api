import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from '../orders/order.entity';
import { Product } from '../products/product.entity';
import { Price } from '../prices/price.entity';
import { TimestampColumn } from '../embedded-entities/timestamp.column';

@Entity({ name: 'order_products' })
export class OrderProduct {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Order, order => order.orderProducts)
  @JoinColumn()
  order: Order;

  @ManyToOne(() => Product, product => product.orderProducts)
  @JoinColumn()
  product: Product

  @ManyToOne(() => Price, price => price.orderProduct)
  price: Price

  @Column(() => TimestampColumn)
  timestamp: TimestampColumn;
}

