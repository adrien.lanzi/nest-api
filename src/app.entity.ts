import { Column } from 'typeorm';

export class AppEntity {
  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created: string;

  @Column({ type: 'timestamp', onUpdate: 'NOW()', nullable: true })
  modified: string;
}
