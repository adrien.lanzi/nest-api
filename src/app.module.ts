import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users/users.controller';
import { ProductsModule } from './products/products.module';
import { OrdersModule } from './orders/orders.module';
import { OrderProductsModule } from './order-products/order-products.module';
import { CustomersModule } from './customers/customers.module';
import { AddressesModule } from './addresses/addresses.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { ContactModule } from './contact/contact.module';
import { ConfigModule } from '@nestjs/config';
import { AccountConfirmationModule } from './account-confirmation/account-confirmation.module';
import { DeliveriesModule } from './deliveries/deliveries.module';
import { DeliveryStatusModule } from './delivery-status/delivery-status.module';
import { PricesModule } from './prices/prices.module';
import * as Joi from '@hapi/joi';
import * as path from 'path';
import { EnrollModule } from './enroll/enroll.module';

const ENV = process.env.NODE_ENV;
const distPath = __dirname

@Module(
  imports: [
    TypeOrmModule.forRoot(),
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: process.env.EMAIL_TRANSPORT,
        defaults: {
          from:  `"L'équipe" <${process.env.NOREPLY_EMAIL}>`
        },
        preview: true,
        template: {
          dir: distPath + '/templates',
          adapter: new PugAdapter(),
          options: {
            strict: true
          }
        }
      })
    }),
    AuthModule,
    ConfigModule.forRoot({
      envFilePath: path.resolve(process.cwd(), !ENV ? 'env/.development.env' : `env/.${ENV}.env`),
      isGlobal: true,
      validationSchema: Joi.object({
        NODE_ENV: Joi.string()
          .valid('development', 'production')
          .default('development'),
        PORT: Joi.number().default(3000)
      })
    }),
    UsersModule,
    ProductsModule,
    OrdersModule,
    OrderProductsModule,
    CustomersModule,
    AddressesModule,
    ContactModule,
    AccountConfirmationModule,
    DeliveriesModule,
    DeliveryStatusModule,
    PricesModule,
    EnrollModule
  ],
  controllers: [AppController, UsersController],
  providers: [AppService],
})
export class AppModule {
}
