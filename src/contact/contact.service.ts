import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { ContactInterface } from './contact.interface';

@Injectable()
export class ContactService {
  constructor(private readonly mailerService: MailerService) {}

  public async send(body: ContactInterface): Promise<boolean> {
    const date = new Date();
    const locale = 'fr-FR';
    const frenchFormat = { hour12: false, year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' }
    const dateTimeFormat = new Intl.DateTimeFormat(locale, frenchFormat);
    const [{ value: year },, { value: month },, { value: day },, { value: hour },, { value: minute }] = dateTimeFormat.formatToParts(date);
    const dateDisplay = `${day}/${month}/${year}`;
    const timeDisplay = `${hour}:${minute}`;

    return this.mailerService.sendMail({
      to: body.email,
      subject: 'Nouveau message d\'un utilisateur',
      template: './templates/contact',
      context: { ...body, dateDisplay, timeDisplay }
    })
      .then(() => true)
      .catch((err) => {
        console.log(err);
        return false;
      });
  }
}
