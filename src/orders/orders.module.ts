import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './order.entity';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { Customer } from '../customers/customer.entity';
import { Product } from '../products/product.entity';
import { Address } from '../addresses/address.entity';
import { ProductsService } from '../products/products.service';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Customer, Product, Address])],
  providers: [OrdersService, ProductsService],
  exports: [OrdersService],
  controllers: [OrdersController]
})
export class OrdersModule {}
