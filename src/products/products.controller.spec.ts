import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { Test } from '@nestjs/testing';

describe('ProductsController', () => {
  let productsController: ProductsController;
  let productsService: ProductsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [ProductsService]
    }).compile();

    productsService = moduleRef.get<ProductsService>(ProductsService);
    productsController = moduleRef.get<ProductsController>(ProductsController);
  });

  describe('findById', () => {
    it('Should return an array of products', async () => {
      const result = ['test'];
      jest.spyOn(productsService, 'findById').mockImplementation(() => result);

      expect(await productsController.findById({ id: 1 })).toBe(result);
    });
  });
});