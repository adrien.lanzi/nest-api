import { Body, Controller, Param, Get, Put, Post, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppController } from '../app.controller';
import { OrderDto } from './order.dto';
import { OrdersService } from './orders.service';
import { Order } from './order.entity';
import { CreateOrderDto } from './createOrderDto';
import { Paginate } from '../products/products.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { User as UserEntity } from '../users/user.entity';
import { User } from '../users/user.decorator';
import { Customer as CustomerEntity } from '../customers/customer.entity';
import { Customer } from '../customers/customer.decorator';

@ApiTags('orders')
@Controller('orders')
export class OrdersController extends AppController{
  constructor(private ordersService: OrdersService) {
    super();
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findPaginated(@Query() { take, skip }): Promise<Paginate<Order>> {
    return await this.ordersService.findPaginated({ take, skip });
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async order(@Body() createOrderDto: CreateOrderDto, @User() user: UserEntity): Promise<Order> {
    return await this.ordersService.create(user, createOrderDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/customer')
  async findForCustomer(@Query() { take, skip }, @Customer() customer: CustomerEntity): Promise<Paginate<Order>> {
    return await this.ordersService.findByCustomer(customer.id, { take, skip });
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findByIdForCustomer(@Param('id') id: string, @Customer() customer: CustomerEntity): Promise<Order> {
    return await this.ordersService.findByIdForCustomer(id, customer);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() orderDto: OrderDto): Promise<Order> {
    await this.ordersService.updateOrder(id, orderDto);
    return await this.ordersService.findById(id);
  }
}
