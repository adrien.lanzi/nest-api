import { Controller, Get, Param } from '@nestjs/common';
import { OrderProductService } from './order-product.service';

@Controller('order_products')
export class OrderProductController {
  constructor(private orderProductsService: OrderProductService) {}

  @Get()
  findByOrderId(@Param('orderId') orderId: string) {
    //return this.orderProductsService.findByOrderId(orderId);
  }
}
