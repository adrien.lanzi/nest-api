export interface ContactInterface {
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  isProfessional: boolean;
  companyName?: string;
  subject: string;
  body: string;
}
