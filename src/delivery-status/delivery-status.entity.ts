import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { TimestampColumn } from '../embedded-entities/timestamp.column';
import { Delivery } from '../deliveries/delivery.entity';

export enum DeliveryStatusEnum {
  PENDING = 1,
  ACCEPTED = 2,
  REFUSED = 3,
  DELIVERED = 4,
  CANCELLED = 5
}

@Entity({ name: 'delivery_status' })
export class DeliveryStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @OneToMany(() => Delivery, delivery => delivery.deliveryStatus)
  delivery;

  @Column(() => TimestampColumn)
  timestamp: TimestampColumn;
}
