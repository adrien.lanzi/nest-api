import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { Order } from '../order.entity';
import { OrdersHelper } from '../orders.helper';

@ViewEntity({
  expression: (connection: Connection) => {
    const queryBuilder = connection.createQueryBuilder().from(Order, 'order');


    OrdersHelper.joinAndSelectMostRecentDelivery<Order>(queryBuilder);
    queryBuilder
      .select('order.id', 'id')
      .addSelect('order.customerId', 'customerId')
      .addSelect('delivery.deliveryDate', 'deliveryDate')
      .addSelect('SUM(price.amount)', 'total')
      .addSelect('deliveryType.label', 'deliveryType')
      .addSelect('deliveryStatus.label', 'deliveryStatus')
      .innerJoin('order.orderProducts', 'orderProducts')
      .innerJoin('orderProducts.product', 'products')
      .innerJoin('delivery.deliveryType', 'deliveryType')
      .innerJoin('delivery.deliveryStatus', 'deliveryStatus')
      .innerJoin('products.price', 'price')
      .leftJoin('products.price', 'latest_price', 'price.id < latest_price.id')
      .where('latest_price.id IS NULL')
      .groupBy('order.id')
      .addGroupBy('delivery.deliveryDate')
      .addGroupBy('deliveryType.label')
      .addGroupBy('deliveryStatus.label');

    return queryBuilder;
  }
})
export class OrderCustomerViewEntity {
  @ViewColumn()
  id: number;

  @ViewColumn()
  customerId: number;

  @ViewColumn()
  deliveryDate: Date;

  @ViewColumn()
  total: number;

  @ViewColumn()
  deliveryType: string;

  @ViewColumn()
  deliveryStatus: string;
}
