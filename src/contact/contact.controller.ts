import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ContactService } from './contact.service';
import { ContactInterface } from './contact.interface';

@ApiTags('Contact')
@Controller('contact')
export class ContactController {
  constructor(private contactService: ContactService) {}

  @Post()
  public async post(@Body() body: ContactInterface): Promise<any> {
    return this.contactService.send(body);
  }
}
