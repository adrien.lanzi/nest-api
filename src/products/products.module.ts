import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { PricesService } from '../prices/prices.service';
import { Price } from '../prices/price.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Price])],
  providers: [ProductsService, PricesService],
  exports: [ProductsService],
  controllers: [ProductsController]
})
export class ProductsModule {}
