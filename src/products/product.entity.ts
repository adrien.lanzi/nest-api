import { Column, Entity, ManyToMany, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Price } from '../prices/price.entity';
import { OrderProduct } from '../order-products/order-product.entity';

@Entity({ name: 'products' })
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  label: string;

  @Column({ default: true })
  available: boolean;

  @Column()
  filename: string;

  @OneToMany(() => Price, prices => prices.product, { cascade: true })
  prices: Price[];

  @ManyToMany(() => OrderProduct, orderProduct => orderProduct.product)
  orderProducts: OrderProduct[];

  @OneToOne(() => Price, currentPrice => currentPrice.product, { cascade: true })
  price: Price;
}
