import { Body, Controller, Get, HttpException, HttpStatus, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CustomersService } from './customers.service';
import { Address } from '../addresses/address.entity';
import { AddressesService } from '../addresses/addresses.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Customer } from './customer.decorator';
import { Customer as CustomerEntity } from './customer.entity';
import { RolesGuard } from '../roles.guard';
import { UserRole } from '../users/user.entity';
import { Roles } from '../roles.decorator';
import { Paginate } from '../products/products.service';
import { Order } from '../orders/order.entity';
import { OrdersService } from '../orders/orders.service';
import { AddressDto } from '../addresses/address.dto';

@ApiTags('Customers')
@Controller('customers')
export class CustomersController {
  constructor(private customersService: CustomersService, private addressesService: AddressesService, private ordersService: OrdersService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/addresses')
  async getAddresses(@Customer() customer: CustomerEntity): Promise<Address[]> {
    if (!customer) {
      throw new HttpException('Bad customer', HttpStatus.BAD_REQUEST);
    }

    return await this.addressesService.getAddressesByCustomerId(customer.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/addresses')
  async saveAddress(@Customer() customer: CustomerEntity, @Body() addressDto: AddressDto): Promise<Address> {
    const payload = { ...addressDto, customer };
    return await this.addressesService.post(payload);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @Get(':id')
  async getOne(@Param('id') id: number): Promise<CustomerEntity> {
    return await this.customersService.get(id);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @Get()
  async getAll(): Promise<Paginate<CustomerEntity>> {
    return await this.customersService.getAll();
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @Get(':id/orders')
  async getOrders(@Param('id') id: number): Promise<Paginate<Order>> {
    return await this.ordersService.findByCustomer(id);
  }

  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(UserRole.ADMIN)
  @Patch(':id/restriction')
  async restrictCustomer(@Param('id') id: number, @Body() body: { isRestricted: boolean }): Promise<boolean> {
    return await this.customersService.setRestriction(id, body.isRestricted);
  }
}
