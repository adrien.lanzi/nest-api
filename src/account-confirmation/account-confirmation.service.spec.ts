import { Test, TestingModule } from '@nestjs/testing';
import { AccountConfirmationService } from './account-confirmation.service';

describe('AccountConfirmationService', () => {
  let service: AccountConfirmationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AccountConfirmationService],
    }).compile();

    service = module.get<AccountConfirmationService>(AccountConfirmationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
